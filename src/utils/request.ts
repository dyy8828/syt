import { ElMessage } from 'element-plus';
//对于axios 函数库进行二次封装？
//二次封装的目的
//1.利用axios请求 响应拦截器功能
//2.请求拦截器 一般可以在请求头中携带公共的参数 token
//3.相应拦截器，可以简化服务器返回的数据
import axios from "axios";

//利用axios.create 方法创建一个axios实例 可以设置基础路径、超时的事件的设置

const request = axios.create({
  baseURL: "/api", //请求的基础路径参数
  timeout: 5000, //超时时间设置 超出5秒失败
});

//请求拦截器
request.interceptors.request.use((config) => {
  //config: 请求拦截器回调注入的对象(配置对象)，配置对象
  //最重要的时headers属性
  //可以通过请求头携带公共参数-token
  return config;
});

//相应拦截器
request.interceptors.response.use(
  (response) => {
    // 相应拦截器成功的回调 一般会进行简化数据
    return response;
  },
  (error) => {
    //处理http网络错误
    let status = error.response;
    console.log(error);
    switch (status) {
       case 404:
        ElMessage({
          type: 'error',
          message:'请求失败，路径出现问题'
        })
        break;
    }

    return Promise.reject(new Error(error.message));
  }
);

//务必对外暴露axios
export default request;
