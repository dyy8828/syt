import { createApp } from 'vue'

//引入清楚默认样式
import '@/style/reset.scss'
//引入全局组件--顶部 底部都是全局组件
import HospitalTop from '@/components/hospital_top/index.vue'

import HospitalBottom from '@/components/hospital_botton/index.vue'

//引入vue-router核心插件并且安装
import router from '@/router'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
//国际化
//@ts-ignore
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'

import App from '@/App.vue'


//利用createApp方法创建应用实例 并且将应用实例挂在到挂载点上
const app =createApp(App)
app.component('HospitalTop',HospitalTop);
app.component('HospitalBottom',HospitalBottom)
//安装vue-router
app.use(router)
app.use(ElementPlus, {
    locale: zhCn,
  })
//挂载
app.mount('#app')
